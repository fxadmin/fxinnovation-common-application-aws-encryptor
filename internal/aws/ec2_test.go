//Package aws common
package aws_test

import (
	"os/user"
	"testing"
	"time"

	"bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/information"
	internalaws "bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/internal/aws"
	"bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/testing/aws/mock"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/stretchr/testify/assert"
)

//tags create common
func tags(now time.Time, volumeID, deviceName, instanceID, username, tagTracker string) []*ec2.Tag {
	return []*ec2.Tag{
		{Key: aws.String("source-volume-id"), Value: aws.String(volumeID)},
		{Key: aws.String("source-device-name"), Value: aws.String(deviceName)},
		{Key: aws.String("source-instance-id"), Value: aws.String(instanceID)},

		{Key: aws.String(internalaws.KeyTagEncryptor), Value: aws.String(tagTracker)},
		{Key: aws.String("process-user"), Value: aws.String(username)},
		{Key: aws.String("aws-encryptor-starting-date"), Value: aws.String(now.Format(time.RFC3339))},
		{Key: aws.String("aws-encryptor-git-commit"), Value: aws.String(information.GitCommit)},
		{Key: aws.String("aws-encryptor-git-describe"), Value: aws.String(information.GitDescribe)},
		{Key: aws.String("aws-encryptor-git-dirty"), Value: aws.String(information.GitDirty)},
		{Key: aws.String("aws-encryptor-buildtime"), Value: aws.String(information.BuildTime)},
	}
}

//scenarioOK we have:
// - instance i-1 with 3 volumes root-1-1, unencrypted vol-dn-1-1, encrypted  vol-dn-1-2
// - instance i-2 with 2 volumes root-2-1, unencrypted vol-dn-2-1
func scenarioOK(t *testing.T, now time.Time, region string, filters []*ec2.Filter, tagTracker string) ec2iface.EC2API {
	currentUser, _ := user.Current()
	username := currentUser.Name

	return &mock.EC2Client{
		T: t,
		//first call describe instances
		DescribeInstancesValues: []*mock.Result{
			{
				Input: &ec2.DescribeInstancesInput{
					Filters: filters,
				},
				Output: &ec2.DescribeInstancesOutput{
					Reservations: []*ec2.Reservation{
						{
							Instances: []*ec2.Instance{
								{
									InstanceId:     aws.String("i-1"),
									RootDeviceName: aws.String("root-1-1"),
									BlockDeviceMappings: []*ec2.InstanceBlockDeviceMapping{
										{
											DeviceName: aws.String("root-1-1"),
											Ebs: &ec2.EbsInstanceBlockDevice{
												VolumeId: aws.String("vol-root-1"),
											},
										},
										{
											DeviceName: aws.String("dn-1-1"),
											Ebs: &ec2.EbsInstanceBlockDevice{
												VolumeId: aws.String("vol-dn-1-1"),
											},
										},
										{
											DeviceName: aws.String("dn-1-2"),
											Ebs: &ec2.EbsInstanceBlockDevice{
												VolumeId: aws.String("vol-dn-1-2"),
											},
										},
									},
								},
								{
									InstanceId:     aws.String("i-2"),
									RootDeviceName: aws.String("root-2-1"),
									BlockDeviceMappings: []*ec2.InstanceBlockDeviceMapping{
										{
											DeviceName: aws.String("root-2-1"),
											Ebs: &ec2.EbsInstanceBlockDevice{
												VolumeId: aws.String("vol-root-2"),
											},
										},
										{
											DeviceName: aws.String("dn-2-1"),
											Ebs: &ec2.EbsInstanceBlockDevice{
												VolumeId: aws.String("vol-dn-2-1"),
											},
										},
									},
								},
							},
						},
					},
				},
				Err: nil,
			},
		},
		DescribeVolumesValues: []*mock.Result{
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-dn-1-1"}),
				},
				Output: &ec2.DescribeVolumesOutput{
					Volumes: []*ec2.Volume{
						{
							VolumeId:         aws.String("vol-dn-1-1"),
							AvailabilityZone: aws.String("us-east-1a"),
							Encrypted:        aws.Bool(false),
							Iops:             aws.Int64(42),
							Size:             aws.Int64(666),
							KmsKeyId:         aws.String("vol-dn-1-1-kms"),
							VolumeType:       aws.String("vol-dn-1-1-type"),
							Tags: []*ec2.Tag{
								{Key: aws.String("Name"), Value: aws.String("vol-dn-1-1")},
							},
						},
					},
				},
				Err: nil,
			},
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-dn-1-2"}),
				},
				Output: &ec2.DescribeVolumesOutput{
					Volumes: []*ec2.Volume{
						{
							VolumeId:         aws.String("vol-dn-1-2"),
							AvailabilityZone: aws.String("us-east-1a"),
							Encrypted:        aws.Bool(true),
							Tags: []*ec2.Tag{
								{Key: aws.String("Name"), Value: aws.String("vol-dn-1-2")},
							},
						},
					},
				},
				Err: nil,
			},
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-dn-2-1"}),
				},
				Output: &ec2.DescribeVolumesOutput{
					Volumes: []*ec2.Volume{
						{
							VolumeId:         aws.String("vol-dn-2-1"),
							AvailabilityZone: aws.String("us-east-1a"),
							Encrypted:        aws.Bool(false),
							Iops:             aws.Int64(42),
							Size:             aws.Int64(666),
							KmsKeyId:         nil,
							VolumeType:       aws.String(ec2.VolumeTypeIo1),
							Tags: []*ec2.Tag{
								{Key: aws.String("Name"), Value: aws.String("vol-dn-2-1")},
								{Key: aws.String("PersonalTag"), Value: aws.String("PersonalValue")},
								{Key: aws.String("aws-encryptor-buildtime"), Value: aws.String("PersonalValue")},
								{Key: aws.String("aws:internal"), Value: aws.String("no copy")},
							},
						},
					},
				},
				Err: nil,
			},
		},
		StopInstancesValues: []*mock.Result{
			{
				Input: &ec2.StopInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-1"}),
				},
				Output: &ec2.StopInstancesOutput{},
				Err:    nil,
			},
			{
				Input: &ec2.StopInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-2"}),
				},
				Output: &ec2.StopInstancesOutput{},
				Err:    nil,
			},
		},
		WaitUntilInstanceStoppedValues: []*mock.Result{
			{
				Input: &ec2.DescribeInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-1"}),
				},
			},
			{
				Input: &ec2.DescribeInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-2"}),
				},
			},
		},
		CreateSnapshotValues: []*mock.Result{
			{
				Input: &ec2.CreateSnapshotInput{
					VolumeId:    aws.String("vol-dn-1-1"),
					Description: aws.String("Create Snapshot - i-1"),
				},
				Output: &ec2.Snapshot{
					SnapshotId: aws.String("snap-vol-dn-1-1"),
				},
			},
			{
				Input: &ec2.CreateSnapshotInput{
					VolumeId:    aws.String("vol-dn-2-1"),
					Description: aws.String("Create Snapshot - i-2"),
				},
				Output: &ec2.Snapshot{
					SnapshotId: aws.String("snap-vol-dn-2-1"),
				},
			},
		},
		CopySnapshotValues: []*mock.Result{
			{
				Input: &ec2.CopySnapshotInput{
					Encrypted:        aws.Bool(true),
					SourceSnapshotId: aws.String("snap-vol-dn-1-1"),
					SourceRegion:     aws.String(region),
					Description:      aws.String("Encrypt Snapshot - i-1"),
				},
				Output: &ec2.CopySnapshotOutput{
					SnapshotId: aws.String("encrypted-snap-vol-dn-1-1"),
				},
			},
			{
				Input: &ec2.CopySnapshotInput{
					Encrypted:        aws.Bool(true),
					SourceSnapshotId: aws.String("snap-vol-dn-2-1"),
					SourceRegion:     aws.String(region),
					Description:      aws.String("Encrypt Snapshot - i-2"),
				},
				Output: &ec2.CopySnapshotOutput{
					SnapshotId: aws.String("encrypted-snap-vol-dn-2-1"),
				},
			},
		},
		CreateVolumeValues: []*mock.Result{
			{
				Input: &ec2.CreateVolumeInput{
					SnapshotId:       aws.String("encrypted-snap-vol-dn-1-1"),
					AvailabilityZone: aws.String("us-east-1a"),
					Iops:             nil,
					Size:             aws.Int64(666),
					KmsKeyId:         aws.String("vol-dn-1-1-kms"),
					VolumeType:       aws.String("vol-dn-1-1-type"),
					TagSpecifications: []*ec2.TagSpecification{{
						ResourceType: aws.String("volume"),
						Tags: append(tags(now, "vol-dn-1-1", "dn-1-1", "i-1", username, tagTracker),
							&ec2.Tag{Key: aws.String("Name"), Value: aws.String("vol-dn-1-1")}),
					}},
				},
				Output: &ec2.Volume{
					VolumeId: aws.String("vol-encrypted-dn-1-1"),
				},
			},
			{
				Input: &ec2.CreateVolumeInput{
					SnapshotId:       aws.String("encrypted-snap-vol-dn-2-1"),
					AvailabilityZone: aws.String("us-east-1a"),
					Iops:             aws.Int64(42),
					Size:             aws.Int64(666),
					KmsKeyId:         nil,
					VolumeType:       aws.String(ec2.VolumeTypeIo1),
					TagSpecifications: []*ec2.TagSpecification{{
						ResourceType: aws.String("volume"),
						Tags: append(tags(now, "vol-dn-2-1", "dn-2-1", "i-2", username, tagTracker),
							&ec2.Tag{Key: aws.String("PersonalTag"), Value: aws.String("PersonalValue")},
							&ec2.Tag{Key: aws.String("Name"), Value: aws.String("vol-dn-2-1")}),
					}},
				},
				Output: &ec2.Volume{
					VolumeId: aws.String("vol-encrypted-dn-2-1"),
				},
			},
		},
		DetachVolumeValues: []*mock.Result{
			{
				Input: &ec2.DetachVolumeInput{
					InstanceId: aws.String("i-1"),
					VolumeId:   aws.String("vol-dn-1-1"),
					Device:     aws.String("dn-1-1"),
				},
				Output: &ec2.VolumeAttachment{},
			},
			{
				Input: &ec2.DetachVolumeInput{
					InstanceId: aws.String("i-2"),
					VolumeId:   aws.String("vol-dn-2-1"),
					Device:     aws.String("dn-2-1"),
				},
				Output: &ec2.VolumeAttachment{},
			},
		},
		AttachVolumeValues: []*mock.Result{
			{
				Input: &ec2.AttachVolumeInput{
					InstanceId: aws.String("i-1"),
					VolumeId:   aws.String("vol-encrypted-dn-1-1"),
					Device:     aws.String("dn-1-1"),
				},
				Output: &ec2.VolumeAttachment{},
			},
			{
				Input: &ec2.AttachVolumeInput{
					InstanceId: aws.String("i-2"),
					VolumeId:   aws.String("vol-encrypted-dn-2-1"),
					Device:     aws.String("dn-2-1"),
				},
				Output: &ec2.VolumeAttachment{},
			},
		},

		CreateTagsValues: []*mock.Result{
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"snap-vol-dn-1-1"}),
					Tags: append(tags(now, "vol-dn-1-1", "dn-1-1", "i-1", username, tagTracker),
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("cs-vol-dn-1-1")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"encrypted-snap-vol-dn-1-1"}),
					Tags: append(tags(now, "vol-dn-1-1", "dn-1-1", "i-1", username, tagTracker),
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("es-encrypted-snap-vol-dn-1-1")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"vol-dn-1-1"}),
					Tags: append(tags(now, "vol-dn-1-1", "dn-1-1", "i-1", username, tagTracker),
						&ec2.Tag{Key: aws.String("aws-encryptor-done"), Value: aws.String("true")},
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("vol-dn-1-1-detached")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"snap-vol-dn-2-1"}),
					Tags: append(tags(now, "vol-dn-2-1", "dn-2-1", "i-2", username, tagTracker),
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("cs-vol-dn-2-1")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"encrypted-snap-vol-dn-2-1"}),
					Tags: append(tags(now, "vol-dn-2-1", "dn-2-1", "i-2", username, tagTracker),
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("es-encrypted-snap-vol-dn-2-1")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
			{
				Input: &ec2.CreateTagsInput{
					Resources: aws.StringSlice([]string{"vol-dn-2-1"}),
					Tags: append(tags(now, "vol-dn-2-1", "dn-2-1", "i-2", username, tagTracker),
						&ec2.Tag{Key: aws.String("aws-encryptor-done"), Value: aws.String("true")},
						&ec2.Tag{Key: aws.String("Name"), Value: aws.String("vol-dn-2-1-detached")}),
				},
				Output: &ec2.CreateTagsOutput{},
			},
		},
		WaitUntilSnapshotCompletedValues: []*mock.Result{
			{
				Input: &ec2.DescribeSnapshotsInput{
					SnapshotIds: aws.StringSlice([]string{"snap-vol-dn-1-1"}),
				},
			},
			{

				Input: &ec2.DescribeSnapshotsInput{
					SnapshotIds: aws.StringSlice([]string{"encrypted-snap-vol-dn-1-1"}),
				},
			},
			{
				Input: &ec2.DescribeSnapshotsInput{
					SnapshotIds: aws.StringSlice([]string{"snap-vol-dn-2-1"}),
				},
			},
			{
				Input: &ec2.DescribeSnapshotsInput{
					SnapshotIds: aws.StringSlice([]string{"encrypted-snap-vol-dn-2-1"}),
				},
			},
		},
		WaitUntilVolumeAvailableValues: []*mock.Result{
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-encrypted-dn-1-1"}),
				},
			},
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-dn-1-1"}),
				},
			},
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-encrypted-dn-2-1"}),
				},
			},

			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-dn-2-1"}),
				},
			},
		},
		WaitUntilVolumeInUseValues: []*mock.Result{
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-encrypted-dn-1-1"}),
				},
			},
			{
				Input: &ec2.DescribeVolumesInput{
					VolumeIds: aws.StringSlice([]string{"vol-encrypted-dn-2-1"}),
				},
			},
		},
		StartInstancesValues: []*mock.Result{
			{
				Input: &ec2.StartInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-1"}),
				},
				Output: &ec2.StartInstancesOutput{},
				Err:    nil,
			},
			{
				Input: &ec2.StartInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-2"}),
				},
				Output: &ec2.StartInstancesOutput{},
				Err:    nil,
			},
		},
		WaitUntilInstanceRunningValues: []*mock.Result{
			{
				Input: &ec2.DescribeInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-1"}),
				},
			},
			{
				Input: &ec2.DescribeInstancesInput{
					InstanceIds: aws.StringSlice([]string{"i-2"}),
				},
			},
		},
	}
}

func TestVolumesDataOK(t *testing.T) {
	filters := []*ec2.Filter{
		{Name: aws.String("tag:Name"), Values: aws.StringSlice([]string{"pr-was"})},
	}
	paramVolumeData := map[string][]string{
		"tag:Name": {"pr-was"},
	}
	tagTracker := "common-tag"
	region := "us-east-1"
	now := time.Now()

	mockEC2 := scenarioOK(t, now, region, filters, tagTracker)

	encrypter := &internalaws.Encrypter{
		Force:   true,
		CVS:     mockEC2,
		Region:  region,
		Tag:     tagTracker,
		Encrypt: true,
	}

	err := encrypter.VolumesData(now, paramVolumeData)

	assert.NoError(t, err)
	assert.Empty(t, mockEC2.(*mock.EC2Client).DescribeVolumesValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).CreateTagsValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).StopInstancesValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).WaitUntilInstanceStoppedValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).CreateSnapshotValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).WaitUntilSnapshotCompletedValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).CopySnapshotValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).CreateVolumeValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).DetachVolumeValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).WaitUntilVolumeAvailableValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).AttachVolumeValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).WaitUntilVolumeInUseValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).StartInstancesValues)
	assert.Empty(t, mockEC2.(*mock.EC2Client).WaitUntilInstanceRunningValues)
}
