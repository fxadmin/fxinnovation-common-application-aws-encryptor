//Package aws common
package aws

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os/user"
	"strings"
	"time"

	"bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/information"
	internalcmd "bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/internal/cmd"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
)

//startingDateContextKey we put in context the starting date.
type startingDateContextKey string

const (
	//KeyTagEncryptor it is put on every creation
	KeyTagEncryptor = "aws-encryptor"

	//unknown when we don't have the information
	unknown                                    = "-"
	sep                                        = "-"
	keyTagName                                 = "Name"
	keyTagDone                                 = "aws-encryptor-done"
	suffixDetached                             = "-detached"
	contextStartingDate startingDateContextKey = "date"
)

//volume information about the volume
type volume struct {
	volumeID         string
	deviceName       string
	tags             []*ec2.Tag
	availabilityZone string
	encrypted        bool
	volumeType       *string
	iops             *int64
	kmsKeyID         *string
	size             *int64
}

func (vol *volume) String() string {
	return fmt.Sprint(vol.volumeID, ",", aws.Int64Value(vol.size), ",", vol.encrypted)
}

//instanceVolume the information' volume
type instanceVolume struct {
	vol        *volume
	instanceID string
	name       string
}

func (v *instanceVolume) String() string {
	return fmt.Sprint(v.instanceID, ",", v.name, ",", v.vol.String())
}

//Encrypter manage the volumes
type Encrypter struct {
	//CVS interface with EC2
	CVS ec2iface.EC2API
	//Region on EC2
	Region string
	//Tag will put on each created volume and created snaphost
	Tag string
	//Encrypt encrypt the volume
	Encrypt bool
	//Force without confirmation
	Force bool
	//MaxAttempts for waitXXX
	MaxAttempts int
	//WaiterDelay for WaiterDelay
	WaiterDelay time.Duration
}

//VolumesData for each filtered instances for each its volumes apply the action
// now the datetime
// filters which image we want to inspect its volumes
// tag the tag will be added everywhere
// encrypt true we run the process, false we print the information on volume only
func (c *Encrypter) VolumesData(now time.Time, filters map[string][]string) error {
	const (
		tagNameKey = "Name"
	)
	con := context.WithValue(context.Background(), contextStartingDate, now.Format(time.RFC3339))
	input := describeInstancesInput(filters)
	for {
		output, err := c.CVS.DescribeInstancesWithContext(con, input)
		if err != nil {
			return err
		}
		if output != nil {
			for _, reservation := range output.Reservations {
				for _, instance := range reservation.Instances {
					name := findValue(instance.Tags, tagNameKey, unknown)
					for _, block := range instance.BlockDeviceMappings {
						if err := c.processVolume(con, instance, name, block); err != nil {
							return err
						}
					}
				}

			}
		}
		if output.NextToken == nil {
			break
		}
		input.SetNextToken(*output.NextToken)
	}
	return nil
}

//isRootDevice if the rootDeviceName is egal to blockDeviceName the block device name is the root.
func isRootDevice(rootDeviceName *string, blockDeviceName *string) bool {
	return *(rootDeviceName) == *(blockDeviceName)
}

//processVolume process the volume encrypt if encrypt is true
func (c *Encrypter) processVolume(con context.Context, instance *ec2.Instance, name *string, block *ec2.InstanceBlockDeviceMapping) error {
	if block.Ebs == nil {
		return fmt.Errorf("%v - empty block Ebs", instance)
	}
	//we don't care root
	if !isRootDevice(instance.RootDeviceName, block.DeviceName) {
		vol, err := c.describeVolume(con, block.Ebs.VolumeId)
		vol.deviceName = *block.DeviceName
		if err != nil {
			return err
		}

		instVolume := &instanceVolume{
			instanceID: *instance.InstanceId,
			name:       *name,
			vol:        vol,
		}

		fmt.Println(instVolume.String())
		if c.Encrypt {
			if err := c.encrypt(con, instVolume, c.Tag); err != nil {
				return err
			}
		}

	}
	return nil
}

//tags creates default tags.
func tags(context aws.Context, v *instanceVolume, tag string) []*ec2.Tag {
	const (
		keyTagUser             = "process-user"
		keyTagSourceInstanceID = "source-instance-id"
		keyTagSourceVolumeID   = "source-volume-id"
		keyTagSourceDeviceName = "source-device-name"
		keyTagStartingDate     = "aws-encryptor-starting-date"
		keyTagGitCommit        = "aws-encryptor-git-commit"
		keyTagGitDescribe      = "aws-encryptor-git-describe"
		keyTagGitDirty         = "aws-encryptor-git-dirty"
		keyTagBuildTime        = "aws-encryptor-buildtime"
	)
	startingDate := context.Value(contextStartingDate)

	var username string
	if currentUser, err := user.Current(); err == nil {
		username = currentUser.Name
	} else {
		username = unknown
	}

	return []*ec2.Tag{
		//comm
		{Key: aws.String(keyTagStartingDate), Value: aws.String(startingDate.(string))},
		{Key: aws.String(keyTagUser), Value: aws.String(fmt.Sprint(username))},
		{Key: aws.String(KeyTagEncryptor), Value: aws.String(fmt.Sprint(tag))},

		{Key: aws.String(keyTagGitCommit), Value: aws.String(information.GitCommit)},
		{Key: aws.String(keyTagGitDescribe), Value: aws.String(information.GitDescribe)},
		{Key: aws.String(keyTagGitDirty), Value: aws.String(information.GitDirty)},
		{Key: aws.String(keyTagBuildTime), Value: aws.String(information.BuildTime)},

		{Key: aws.String(keyTagSourceInstanceID), Value: aws.String(fmt.Sprint(v.instanceID))},
		{Key: aws.String(keyTagSourceVolumeID), Value: aws.String(fmt.Sprint(v.vol.volumeID))},
		{Key: aws.String(keyTagSourceDeviceName), Value: aws.String(fmt.Sprint(v.vol.deviceName))},
	}
}

func (c *Encrypter) stopInstance(context aws.Context, v *instanceVolume) error {
	log.Printf("stopInstance instanceID:%v\n", *v)
	_, err := c.CVS.StopInstancesWithContext(context, &ec2.StopInstancesInput{
		InstanceIds: aws.StringSlice([]string{v.instanceID}),
	})
	if err != nil {
		return err
	}
	log.Printf("WaitUntilInstanceStopped instanceID:%v\n", *v)
	err = c.CVS.WaitUntilInstanceStoppedWithContext(context, &ec2.DescribeInstancesInput{
		InstanceIds: aws.StringSlice([]string{v.instanceID}),
	}, c.waiterOptions()...)
	return err
}

func (c *Encrypter) startInstance(context aws.Context, v *instanceVolume) error {
	log.Printf("startInstance instanceID:%v\n", *v)
	_, err := c.CVS.StartInstancesWithContext(context, &ec2.StartInstancesInput{
		InstanceIds: aws.StringSlice([]string{v.instanceID}),
	})
	if err != nil {
		return err
	}
	log.Printf("WaitUntilInstanceRunning instanceID:%v\n", *v)
	err = c.CVS.WaitUntilInstanceRunningWithContext(context, &ec2.DescribeInstancesInput{
		InstanceIds: aws.StringSlice([]string{v.instanceID}),
	}, c.waiterOptions()...)
	return err
}

func (c *Encrypter) createSnapshot(context aws.Context, tags []*ec2.Tag, v *instanceVolume) (*string, error) {
	log.Printf("createSnapshot instanceVolume:%v\n", *v)
	const description = "Create Snapshot - "
	const prefix = "cs"
	snap, err := c.CVS.CreateSnapshotWithContext(context, &ec2.CreateSnapshotInput{
		VolumeId:    aws.String(v.vol.volumeID),
		Description: aws.String(fmt.Sprint(description, v.instanceID)),
	})
	if err != nil {
		return nil, err
	}

	log.Printf("WaitUntilSnapshotCompleted snap.SnapshotId:%q - v:%v\n", *snap.SnapshotId, *v)
	err = c.CVS.WaitUntilSnapshotCompletedWithContext(context, &ec2.DescribeSnapshotsInput{
		SnapshotIds: []*string{snap.SnapshotId},
	}, c.waiterOptions()...)
	if err != nil {
		return nil, err
	}

	log.Printf("CreateTagsWithContext snap.SnapshotId:%q - v:%v\n", *snap.SnapshotId, *v)
	if _, err := c.CVS.CreateTagsWithContext(context, &ec2.CreateTagsInput{
		Resources: []*string{snap.SnapshotId},
		Tags:      append(tags, &ec2.Tag{Key: aws.String(keyTagName), Value: aws.String(fmt.Sprint(prefix, sep, v.vol.volumeID))}),
	}); err != nil {
		return nil, err
	}

	return snap.SnapshotId, err

}

func (c *Encrypter) encryptSnapshot(context aws.Context, tags []*ec2.Tag, v *instanceVolume, snapshotID *string) (*string, error) {
	const description = "Encrypt Snapshot - "
	log.Printf("encryptSnapshot snapshotID:%q\n", *snapshotID)
	const prefix = "es"
	encryptedSnap, err := c.CVS.CopySnapshotWithContext(context, &ec2.CopySnapshotInput{
		Encrypted:        aws.Bool(true),
		SourceSnapshotId: snapshotID,
		SourceRegion:     aws.String(c.Region),
		Description:      aws.String(fmt.Sprint(description, v.instanceID)),
	})
	if err != nil {
		return nil, err
	}

	log.Printf("WaitUntilSnapshotCompleted encryptedSnap.SnapshotId:%q - snapshotID:%q\n", *encryptedSnap.SnapshotId, *snapshotID)
	err = c.CVS.WaitUntilSnapshotCompletedWithContext(context, &ec2.DescribeSnapshotsInput{
		SnapshotIds: []*string{encryptedSnap.SnapshotId},
	}, c.waiterOptions()...)
	if err != nil {
		return nil, err
	}

	log.Printf("CreateTagsWithContext encryptedSnap.SnapshotId:%q - snapshotID:%q\n", *encryptedSnap.SnapshotId, *snapshotID)
	if _, err := c.CVS.CreateTagsWithContext(context, &ec2.CreateTagsInput{
		Resources: []*string{encryptedSnap.SnapshotId},
		Tags:      append(tags, &ec2.Tag{Key: aws.String(keyTagName), Value: aws.String(fmt.Sprint(prefix, sep, *encryptedSnap.SnapshotId))}),
	}); err != nil {
		return nil, err
	}

	return encryptedSnap.SnapshotId, err
}

//contains the tag
func contains(tag *ec2.Tag, tags []*ec2.Tag) bool {
	for _, currentTag := range tags {
		if *tag.Key == *currentTag.Key {
			return true
		}
	}
	return false
}

func (c *Encrypter) createVolume(context aws.Context, tags []*ec2.Tag, v *instanceVolume, encryptedSnapshotID *string) (*string, error) {
	log.Printf("createVolume encryptedSnapshotID:%q - v:%v\n", *encryptedSnapshotID, *v)
	const (
		prefix            = "cv"
		resourceType      = "volume"
		prefixInternalTag = "aws:"
	)

	currentTags := make([]*ec2.Tag, len(tags))
	copy(currentTags, tags)
	for _, tag := range v.vol.tags {
		if !strings.HasPrefix(*tag.Key, prefixInternalTag) && !contains(tag, tags) {
			currentTags = append(currentTags, tag)
		}
	}

	iots := v.vol.iops
	if *v.vol.volumeType != ec2.VolumeTypeIo1 {
		iots = nil
	}

	createVol, err := c.CVS.CreateVolumeWithContext(context, &ec2.CreateVolumeInput{
		SnapshotId:       encryptedSnapshotID,
		AvailabilityZone: aws.String(v.vol.availabilityZone),
		Iops:             iots,
		KmsKeyId:         v.vol.kmsKeyID,
		Size:             v.vol.size,
		VolumeType:       v.vol.volumeType,
		TagSpecifications: []*ec2.TagSpecification{{
			ResourceType: aws.String(resourceType),
			Tags:         currentTags,
		}},
	})
	if err != nil {
		return nil, err
	}
	log.Printf("WaitUntilVolumeAvailable createVol.VolumeId:%q - encryptedSnapshotID:%q - v:%v\n", *createVol.VolumeId, *encryptedSnapshotID, *v)
	err = c.CVS.WaitUntilVolumeAvailableWithContext(context, &ec2.DescribeVolumesInput{
		VolumeIds: []*string{createVol.VolumeId},
	}, c.waiterOptions()...)

	return createVol.VolumeId, err

}

func (c *Encrypter) attachVolume(context aws.Context, v *instanceVolume, volumeID *string) error {
	log.Printf("DetachVolume %q - %v\n", *volumeID, *v)
	_, err := c.CVS.DetachVolumeWithContext(context, &ec2.DetachVolumeInput{
		InstanceId: aws.String(v.instanceID),
		VolumeId:   aws.String(v.vol.volumeID),
		Device:     aws.String(v.vol.deviceName),
	})
	if err != nil {
		return err
	}
	log.Printf("WaitUntilVolumeAvailable volumeID:%q - v:%v\n", *volumeID, *v)
	if err := c.CVS.WaitUntilVolumeAvailableWithContext(context, &ec2.DescribeVolumesInput{
		VolumeIds: []*string{aws.String(v.vol.volumeID)},
	}, c.waiterOptions()...); err != nil {
		return err
	}

	log.Printf("AttachVolume volumeID:%q - v:%v\n", *volumeID, *v)
	_, err = c.CVS.AttachVolumeWithContext(context, &ec2.AttachVolumeInput{
		InstanceId: aws.String(v.instanceID),
		VolumeId:   volumeID,
		Device:     aws.String(v.vol.deviceName),
	})
	if err != nil {
		return err
	}

	log.Printf("WaitUntilVolumeInUse volumeID:%q - v:%v\n", *volumeID, *v)
	return c.CVS.WaitUntilVolumeInUseWithContext(context, &ec2.DescribeVolumesInput{
		VolumeIds: []*string{volumeID},
	}, c.waiterOptions()...)
}

//Encrypt return the action to encrypt a volume
func (c *Encrypter) encrypt(context aws.Context, v *instanceVolume, tag string) error {
	if !v.vol.encrypted {
		if !c.Force {
			if yes, err := internalcmd.AskForConfirmation(); !yes || err != nil {
				return err
			}
		}

		tags := tags(context, v, tag)

		if err := c.stopInstance(context, v); err != nil {
			return err
		}

		snapshotID, err := c.createSnapshot(context, tags, v)
		if err != nil {
			return err
		}

		encryptedSnapshotID, err := c.encryptSnapshot(context, tags, v, snapshotID)
		if err != nil {
			return err
		}

		volumeID, err := c.createVolume(context, tags, v, encryptedSnapshotID)
		if err != nil {
			return err
		}

		if err := c.attachVolume(context, v, volumeID); err != nil {
			return err
		}

		name := *findValue(v.vol.tags, keyTagName, v.vol.volumeID)
		if _, err := c.CVS.CreateTagsWithContext(context, &ec2.CreateTagsInput{
			Resources: []*string{aws.String(v.vol.volumeID)},
			Tags: append(tags,
				&ec2.Tag{Key: aws.String(keyTagName), Value: aws.String(fmt.Sprint(name, suffixDetached))},
				&ec2.Tag{Key: aws.String(keyTagDone), Value: aws.String(fmt.Sprint(true))},
			),
		}); err != nil {
			return err
		}

		if err := c.startInstance(context, v); err != nil {
			return err
		}

	}
	return nil

}

func describeInstancesInput(filters map[string][]string) *ec2.DescribeInstancesInput {
	lenFilters := len(filters)
	if lenFilters == 0 {
		return &ec2.DescribeInstancesInput{}
	}

	awsFilters := make([]*ec2.Filter, lenFilters)

	i := 0
	for k, v := range filters {
		awsFilters[i] = &ec2.Filter{
			Name:   aws.String(k),
			Values: aws.StringSlice(v),
		}
		i++
	}
	return &ec2.DescribeInstancesInput{
		Filters: awsFilters,
	}
}

//findValue find the tag and return the associated value if not found returns the default value
func findValue(tags []*ec2.Tag, tagKey string, defaultValue string) *string {
	for _, tag := range tags {
		if *tag.Key == tagKey {
			return tag.Value
		}
	}
	return aws.String(defaultValue)

}

//describeVolume get the information on volume
func (c *Encrypter) describeVolume(context aws.Context, volumeID *string) (*volume, error) {

	input := &ec2.DescribeVolumesInput{
		VolumeIds: []*string{volumeID},
	}

	output, err := c.CVS.DescribeVolumesWithContext(context, input)
	if err != nil {
		return nil, err
	}

	if len(output.Volumes) == 1 {
		return &volume{
			volumeID:         *output.Volumes[0].VolumeId,
			tags:             output.Volumes[0].Tags,
			availabilityZone: *output.Volumes[0].AvailabilityZone,
			encrypted:        *output.Volumes[0].Encrypted,
			volumeType:       output.Volumes[0].VolumeType,
			iops:             output.Volumes[0].Iops,
			kmsKeyID:         output.Volumes[0].KmsKeyId,
			size:             output.Volumes[0].Size,
		}, nil
	}

	return nil, errors.New("problem")
}

func (c *Encrypter) waiterOptions() []request.WaiterOption {
	return []request.WaiterOption{
		request.WithWaiterMaxAttempts(c.MaxAttempts),
		request.WithWaiterDelay(request.ConstantWaiterDelay(c.WaiterDelay)),
	}
}
