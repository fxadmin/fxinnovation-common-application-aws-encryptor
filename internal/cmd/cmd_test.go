package cmd

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFiltersValue_Set(t *testing.T) {
	tests := []struct {
		name     string
		s        *FiltersValue
		expected *FiltersValue
		arg      string
		wantErr  bool
	}{
		{
			name:     "OK",
			s:        (*FiltersValue)(&map[string][]string{}),
			expected: (*FiltersValue)(&map[string][]string{"key": {"value1", "value2"}}),
			arg:      "key=value1,value2",
			wantErr:  false,
		},
		{
			name:     "OK - the key exists",
			s:        (*FiltersValue)(&map[string][]string{"key": {"value1"}}),
			expected: (*FiltersValue)(&map[string][]string{"key": {"value1", "value2"}}),
			arg:      "key=value2",
			wantErr:  false,
		},
		{
			name:     "OK - Empty key",
			s:        (*FiltersValue)(&map[string][]string{}),
			expected: (*FiltersValue)(&map[string][]string{"key": {""}}),
			arg:      "key=",
			wantErr:  false,
		},
		{
			name:     "KO - Empty arg",
			s:        (*FiltersValue)(&map[string][]string{}),
			expected: (*FiltersValue)(&map[string][]string{}),
			arg:      "",
			wantErr:  true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.s.Set(tt.arg); (err != nil) != tt.wantErr {
				t.Errorf("FiltersValue.Set() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				assert.Equal(t, tt.expected, tt.s)
			}
		})
	}
}
