//Package cmd the utilities for command line
package cmd

import (
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/information"
)

//FiltersValue -- filters Value
type FiltersValue map[string][]string

//NewFiltersValue -param key1=value -param key2=value1,value2
func NewFiltersValue(val map[string][]string, p *map[string][]string) *FiltersValue {
	*p = val
	return (*FiltersValue)(p)
}

//Set Parse the val (ex:key=value1,...,valueN)
//It adds in map the key with the array of values.
//It returns a error if the format is not right
func (s *FiltersValue) Set(val string) error {
	const (
		sepKeyValues = "="
		sepValues    = ","
	)
	keyValues := strings.SplitN(val, sepKeyValues, 2)
	if len(keyValues) != 2 {
		return fmt.Errorf("%q so bad", val)
	}
	key := keyValues[0]
	values := strings.Split(keyValues[1], sepValues)
	filters := map[string][]string(*s)
	if val, ok := filters[key]; ok {
		filters[key] = append(val, values...)
	} else {
		filters[key] = values
	}
	return nil
}

//Get the value
func (s *FiltersValue) Get() interface{} {
	return map[string][]string(*s)
}

//String print the map
func (s *FiltersValue) String() string {
	return fmt.Sprintf("%v", s.Get())
}

//CommandLine the I/O command line
type CommandLine struct {
	Stdout io.Writer
	Stderr io.Writer
	Stdin  *os.File
}

//Init log and print version
func (c *CommandLine) Init(prefix string) *CommandLine {
	c.Stdout = os.Stdout
	c.Stderr = os.Stderr
	c.Stdin = os.Stdin

	log.SetPrefix(prefix)
	log.SetOutput(c.Stderr)
	log.Println(information.Print())
	return c
}

//Fatal return 1 and log in Err
func (c *CommandLine) Fatal(err error) int {
	fmt.Fprintf(c.Stderr, fmt.Sprint(err))
	return 1
}

//AskForConfirmation ask if you want to continue
func AskForConfirmation() (bool, error) {
	const (
		question = "Do you want to continue y/N? "
		yes      = "y"
	)
	fmt.Print(question)
	var response string
	_, err := fmt.Scanln(&response)
	return response == yes, err
}
