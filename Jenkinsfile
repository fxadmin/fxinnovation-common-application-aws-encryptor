#!/usr/bin/env groovy
@Library('com.fxinnovation.fxinnovation-itoa.tools.library@0.2.2') _
node(){
    def srcDir = '/go/src/bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor'
    def osTargets = ['linux', 'windows', 'darwin']
    def goArch = 'amd64'
    def projectName = "fxinnovation-common-application-aws-encryptor"
    def targetDirectory = "target"
    def dockerfile = "Dockerfile.jenkins"

    try{
        pipeline(srcDir, goArch, osTargets, projectName, targetDirectory, dockerfile)
    } catch(exception) {
        stage('Notify') {
            notify('failure', true)
        }
        throw exception
    } finally {
        stage('Clean') {
            cleanWs()
        }
    }
}

/**
 * The pipeline test, build the binaries.
 */
def pipeline(srcDir, goArch, osTargets, projectName, targetDirectory, dockerfile){
    return node(){
        stage('Checkout') {
            checkout scm
        }
        def currentDocker  = docker.build("${projectName}:${env.BUILD_NUMBER}", "-f ${dockerfile} .")

        currentDocker.inside("-v ${pwd()}/${targetDirectory}:${srcDir}/${targetDirectory}") { c ->
            stage("Test") {
                try{
                    sh "cd ${srcDir} && make test-coverage"
                } finally {
                    cobertura coberturaReportFile: "${targetDirectory}/coverage-test.xml"
                }
            }        
            def builders = [:]
            for (os in osTargets) {
                def target = os
                builders[target] = {
                    stage("Build-${target}") {
                            withEnv(["BUILD_GOOS=${target}", "BUILD_GOARCH=${goArch}"]) {
                                sh "cd ${srcDir} && make build"
                                archiveArtifacts "${targetDirectory}/*"
                        }
                    }
                }
            }
            parallel builders

            stage('Notify') {
                notify('success', false)
            }
        }
    }
}
