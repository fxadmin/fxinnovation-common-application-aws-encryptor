# AWS Encryptor

[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor)](https://goreportcard.com/report/bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor)

## Getting started

We use:
* the pattern git flow.
* go dep

```bash
make tools
```

## encryptor Process: encrypt the volume

1. Stop instance.
2. Create an EBS snapshot for each unencrypted volume not root.
    1. Add tag Name with cs-volumeID.
3. Create an encrypted copy of the snapshot.
    1. Add tag Name with es-unencrypted snapshotID.
4. Create a new encrypted volume from each snapshot.
    1. Add all tags of unencrypted volume.
5. Detach each original EBS volume.
6. Attach each new encrypted EBS volume.
7. Start instance.
8. Add tag **aws-encryptor-done** on unencrypted volume and add **-detached** suffix to the name.

The following tags are added to created snapshots and created volumes:

|tag name|description|
|:----:|:----:|
|process-user|Who runs the process|
|source-volume-id|The unencrypted volume id|
|source-instance-id|The instance id|
|source-device-name|The unencrypted device name|
|aws-encryptor|<your tag value (default encryptor)>|
|aws-encryptor-starting-date|the process starting date|
|aws-encryptor-version|build information|
|aws-encryptor-git-commit|build information|
|aws-encryptor-git-describe|build information|
|aws-encryptor-git-dirty|build information|
|aws-encryptor-buildtime|build information|

```bash
aws-encryptor \
    -profile <profile> \
    -region <region> \
    -role-arn <role-arn> \
    -filter platform=windows \
    -filter tag:Name=encryptor \
    -tag to-follow \
    -encrypt
```
