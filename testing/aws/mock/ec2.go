//Package mock mock for ec2iface
package mock

import (
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/request"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/stretchr/testify/assert"
)

//Result the input method and the output.
type Result struct {
	Input  interface{}
	Output interface{}
	Err    error
}

//EC2Client mock for EC2iface
type EC2Client struct {
	//T the testing
	T *testing.T
	ec2iface.EC2API

	//DescribeInstancesValues for WithContext
	DescribeInstancesValues []*Result
	//DescribeVolumesValues for WithContext
	DescribeVolumesValues []*Result

	//CreateTagsValues for WithContext
	CreateTagsValues []*Result

	//StopInstancesValues for WithContext
	StopInstancesValues []*Result
	//WaitUntilInstanceStoppedValues for WithContext
	WaitUntilInstanceStoppedValues []*Result

	//CreateSnapshotValues for WithContext
	CreateSnapshotValues []*Result
	//WaitUntilSnapshotCompletedValues for WithContext
	WaitUntilSnapshotCompletedValues []*Result

	//CopySnapshotValues for WithContext
	CopySnapshotValues []*Result

	//CreateVolumeValues for WithContext
	CreateVolumeValues []*Result

	//DetachVolumeValues for WithContext
	DetachVolumeValues []*Result
	//WaitUntilVolumeAvailableValues for WithContext
	WaitUntilVolumeAvailableValues []*Result

	//AttachVolumeValues for WithContext
	AttachVolumeValues []*Result
	//WaitUntilVolumeInUseValues for WithContext
	WaitUntilVolumeInUseValues []*Result

	//StartInstancesValues for WithContext
	StartInstancesValues []*Result
	//WaitUntilInstanceRunningValues for WithContext
	WaitUntilInstanceRunningValues []*Result
}

//DescribeInstancesWithContext read the Value
func (m *EC2Client) DescribeInstancesWithContext(context aws.Context, di *ec2.DescribeInstancesInput, r ...request.Option) (*ec2.DescribeInstancesOutput, error) {
	result := m.DescribeInstancesValues[0]
	m.DescribeInstancesValues = m.DescribeInstancesValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeInstancesInput)), *di)
	return result.Output.(*ec2.DescribeInstancesOutput), result.Err
}

//DescribeVolumesWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) DescribeVolumesWithContext(context aws.Context, di *ec2.DescribeVolumesInput, r ...request.Option) (*ec2.DescribeVolumesOutput, error) {
	result := m.DescribeVolumesValues[0]
	m.DescribeVolumesValues = m.DescribeVolumesValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeVolumesInput)), *di)
	return result.Output.(*ec2.DescribeVolumesOutput), result.Err
}

//StopInstancesWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) StopInstancesWithContext(context aws.Context, di *ec2.StopInstancesInput, r ...request.Option) (*ec2.StopInstancesOutput, error) {
	result := m.StopInstancesValues[0]
	m.StopInstancesValues = m.StopInstancesValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.StopInstancesInput)), *di)
	return result.Output.(*ec2.StopInstancesOutput), result.Err
}

//WaitUntilInstanceStoppedWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) WaitUntilInstanceStoppedWithContext(con aws.Context, di *ec2.DescribeInstancesInput, re ...request.WaiterOption) error {
	result := m.WaitUntilInstanceStoppedValues[0]
	m.WaitUntilInstanceStoppedValues = m.WaitUntilInstanceStoppedValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeInstancesInput)), *di)
	return result.Err
}

//StartInstancesWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) StartInstancesWithContext(context aws.Context, di *ec2.StartInstancesInput, r ...request.Option) (*ec2.StartInstancesOutput, error) {
	result := m.StartInstancesValues[0]
	m.StartInstancesValues = m.StartInstancesValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.StartInstancesInput)), *di)
	return result.Output.(*ec2.StartInstancesOutput), result.Err
}

//WaitUntilInstanceRunningWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) WaitUntilInstanceRunningWithContext(con aws.Context, di *ec2.DescribeInstancesInput, re ...request.WaiterOption) error {
	result := m.WaitUntilInstanceRunningValues[0]
	m.WaitUntilInstanceRunningValues = m.WaitUntilInstanceRunningValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeInstancesInput)), *di)
	return result.Err
}

//CreateSnapshotWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) CreateSnapshotWithContext(context aws.Context, di *ec2.CreateSnapshotInput, re ...request.Option) (*ec2.Snapshot, error) {
	result := m.CreateSnapshotValues[0]
	m.CreateSnapshotValues = m.CreateSnapshotValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.CreateSnapshotInput)), *di)
	return result.Output.(*ec2.Snapshot), result.Err
}

//WaitUntilSnapshotCompletedWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) WaitUntilSnapshotCompletedWithContext(context aws.Context, di *ec2.DescribeSnapshotsInput, re ...request.WaiterOption) error {
	result := m.WaitUntilSnapshotCompletedValues[0]
	m.WaitUntilSnapshotCompletedValues = m.WaitUntilSnapshotCompletedValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeSnapshotsInput)), *di)
	return result.Err
}

//CreateTagsWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) CreateTagsWithContext(context aws.Context, di *ec2.CreateTagsInput, re ...request.Option) (*ec2.CreateTagsOutput, error) {
	result := m.CreateTagsValues[0]
	m.CreateTagsValues = m.CreateTagsValues[1:]
	nbComparaisonTags := len((*result.Input.(*ec2.CreateTagsInput)).Tags)

	assert.Equal(m.T, nbComparaisonTags, len((*di).Tags))

	for _, tag := range (*result.Input.(*ec2.CreateTagsInput)).Tags {
		for _, iTag := range (*di).Tags {
			if *tag.Key == *iTag.Key {
				nbComparaisonTags--
				assert.Equal(m.T, *tag.Value, *iTag.Value)
			}
		}
	}
	assert.Equal(m.T, nbComparaisonTags, 0)
	return result.Output.(*ec2.CreateTagsOutput), result.Err
}

//CopySnapshotWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) CopySnapshotWithContext(context aws.Context, di *ec2.CopySnapshotInput, re ...request.Option) (*ec2.CopySnapshotOutput, error) {
	result := m.CopySnapshotValues[0]
	m.CopySnapshotValues = m.CopySnapshotValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.CopySnapshotInput)), *di)
	return result.Output.(*ec2.CopySnapshotOutput), result.Err
}

func (m *EC2Client) assertInt64(expected *int64, actual *int64) {
	if expected == nil {
		assert.Nil(m.T, actual)
	} else {
		assert.NotNil(m.T, actual)
		assert.Equal(m.T, *expected, *actual)
	}
}
func (m *EC2Client) assertString(expected *string, actual *string) {
	if expected == nil {
		assert.Nil(m.T, actual)
	} else {
		assert.Equal(m.T, (*expected), (*actual))
	}
}

//CreateVolumeWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) CreateVolumeWithContext(context aws.Context, di *ec2.CreateVolumeInput, re ...request.Option) (*ec2.Volume, error) {
	result := m.CreateVolumeValues[0]
	m.CreateVolumeValues = m.CreateVolumeValues[1:]
	assert.Equal(m.T, (*(result.Input.(*ec2.CreateVolumeInput))).AvailabilityZone, (*di).AvailabilityZone)
	assert.Equal(m.T, (*(result.Input.(*ec2.CreateVolumeInput))).Encrypted, (*di).Encrypted)

	m.assertString((result.Input.(*ec2.CreateVolumeInput)).VolumeType, (*di).VolumeType)
	m.assertString((result.Input.(*ec2.CreateVolumeInput)).KmsKeyId, (*di).KmsKeyId)
	m.assertInt64((result.Input.(*ec2.CreateVolumeInput)).Iops, (*di).Iops)
	m.assertInt64((result.Input.(*ec2.CreateVolumeInput)).Size, (*di).Size)

	assert.Equal(m.T, (*result.Input.(*ec2.CreateVolumeInput)).TagSpecifications[0].ResourceType, (*di).TagSpecifications[0].ResourceType)
	nbComparaisonTags := len((*result.Input.(*ec2.CreateVolumeInput)).TagSpecifications[0].Tags)
	assert.Equal(m.T, nbComparaisonTags, len((*di).TagSpecifications[0].Tags))

	for _, tag := range (*result.Input.(*ec2.CreateVolumeInput)).TagSpecifications[0].Tags {
		for _, iTag := range (*di).TagSpecifications[0].Tags {
			if *tag.Key == *iTag.Key {
				nbComparaisonTags--
				assert.Equal(m.T, *tag.Value, *iTag.Value)
			}
		}
	}
	assert.Equal(m.T, nbComparaisonTags, 0)

	return result.Output.(*ec2.Volume), result.Err
}

//WaitUntilVolumeAvailableWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) WaitUntilVolumeAvailableWithContext(context aws.Context, di *ec2.DescribeVolumesInput, re ...request.WaiterOption) error {
	result := m.WaitUntilVolumeAvailableValues[0]
	m.WaitUntilVolumeAvailableValues = m.WaitUntilVolumeAvailableValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeVolumesInput)), *di)
	return result.Err
}

//DetachVolumeWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) DetachVolumeWithContext(context aws.Context, di *ec2.DetachVolumeInput, re ...request.Option) (*ec2.VolumeAttachment, error) {
	result := m.DetachVolumeValues[0]
	m.DetachVolumeValues = m.DetachVolumeValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DetachVolumeInput)), *di)
	return result.Output.(*ec2.VolumeAttachment), result.Err
}

//AttachVolumeWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) AttachVolumeWithContext(context aws.Context, di *ec2.AttachVolumeInput, re ...request.Option) (*ec2.VolumeAttachment, error) {
	result := m.AttachVolumeValues[0]
	m.AttachVolumeValues = m.AttachVolumeValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.AttachVolumeInput)), *di)
	return result.Output.(*ec2.VolumeAttachment), result.Err
}

//WaitUntilVolumeInUseWithContext remove and read the first element of slice XXXValue
func (m *EC2Client) WaitUntilVolumeInUseWithContext(context aws.Context, di *ec2.DescribeVolumesInput, re ...request.WaiterOption) error {
	result := m.WaitUntilVolumeInUseValues[0]
	m.WaitUntilVolumeInUseValues = m.WaitUntilVolumeInUseValues[1:]
	assert.Equal(m.T, *(result.Input.(*ec2.DescribeVolumesInput)), *di)
	return result.Err
}
