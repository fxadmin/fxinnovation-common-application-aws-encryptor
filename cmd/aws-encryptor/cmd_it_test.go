package main

import (
	"flag"
	"fmt"
	"testing"

	internalaws "bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/internal/aws"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/ec2/ec2iface"
	"github.com/stretchr/testify/assert"
)

type testMainOKFlags struct {
	imageID     string
	subnetID    string
	dryRunClean bool
}

var cmdLine = &commandLine{}
var flags = &testMainOKFlags{}

func init() {
	if !testing.Short() {
		flag.StringVar(&flags.imageID, "imageID", "ami-e0df539f", "the imageID for the test")
		flag.StringVar(&flags.subnetID, "subnetID", "", "the subnetID for the test")
		flag.BoolVar(&flags.dryRunClean, "dry-run-clean", false, "dry run for the cleaning end")

		cmdLine.init()

	}

}

//TestMainOK -subnetID <your subnet> -tag integration-test -timeout 3600
// 1 - Create a instance windows with tag Name encryptor with two unencrypted data volumes
// 2 - Run the process
// 3 - Check the volumes are encrypted.
func TestMainOK(t *testing.T) {
	if testing.Short() {
		t.Skip("Create a instance windows with tag encryptor with two unencrypted data volumes")
	}

	filter := []*ec2.Filter{
		{Name: aws.String(fmt.Sprint("tag:", internalaws.KeyTagEncryptor)), Values: aws.StringSlice([]string{cmdLine.tag})},
	}

	defer clean(t, cmdLine.encrypter.CVS, filter, flags.dryRunClean)
	nbDataVolume := 2
	cmdLine.encrypter.Encrypt = true
	cmdLine.encrypter.Force = true
	reservations, err := cmdLine.encrypter.CVS.RunInstances(&ec2.RunInstancesInput{
		DryRun:       aws.Bool(false),
		ImageId:      aws.String(flags.imageID),
		SubnetId:     aws.String(flags.subnetID),
		InstanceType: aws.String("t2.micro"),
		MinCount:     aws.Int64(1),
		MaxCount:     aws.Int64(1),
		BlockDeviceMappings: []*ec2.BlockDeviceMapping{
			{
				DeviceName: aws.String("/dev/sda1"),
				Ebs: &ec2.EbsBlockDevice{
					VolumeSize:          aws.Int64(30),
					VolumeType:          aws.String("standard"),
					DeleteOnTermination: aws.Bool(true),
				},
			},
			{
				DeviceName: aws.String("xvdb"),
				Ebs: &ec2.EbsBlockDevice{
					Encrypted:           aws.Bool(false),
					VolumeSize:          aws.Int64(8),
					VolumeType:          aws.String("standard"),
					DeleteOnTermination: aws.Bool(true),
				},
			},
			{
				DeviceName: aws.String("xvdc"),
				Ebs: &ec2.EbsBlockDevice{
					Encrypted:           aws.Bool(false),
					VolumeSize:          aws.Int64(8),
					VolumeType:          aws.String("standard"),
					DeleteOnTermination: aws.Bool(true),
				},
			},
		},
		TagSpecifications: []*ec2.TagSpecification{
			{
				ResourceType: aws.String("instance"),
				Tags: []*ec2.Tag{
					{Key: aws.String("Name"), Value: aws.String("aws-encryptor")},
					{Key: aws.String("Test"), Value: aws.String("true")},
					{Key: aws.String(internalaws.KeyTagEncryptor), Value: aws.String(cmdLine.tag)},
				},
			},
			{
				ResourceType: aws.String("volume"),
				Tags: []*ec2.Tag{
					{Key: aws.String("Name"), Value: aws.String("aws-encryptor")},
					{Key: aws.String("Test"), Value: aws.String("true")},
					{Key: aws.String(internalaws.KeyTagEncryptor), Value: aws.String(cmdLine.tag)},
				},
			},
		},
	})
	assert.NoError(t, err)

	err = cmdLine.encrypter.CVS.WaitUntilInstanceRunning(&ec2.DescribeInstancesInput{
		InstanceIds: []*string{reservations.Instances[0].InstanceId},
	})
	assert.NoError(t, err)

	cmdLine.filters = map[string][]string{
		"tag:Name":    {"aws-encryptor"},
		"instance-id": {*reservations.Instances[0].InstanceId},
		"platform":    {"windows"},
	}
	assert.Equal(t, 0, cmdLine.main())

	descripInstancesOutput, err := cmdLine.encrypter.CVS.DescribeInstances(&ec2.DescribeInstancesInput{
		InstanceIds: []*string{reservations.Instances[0].InstanceId},
	})
	assert.NoError(t, err)

	vIDs := getVolumeIDs(descripInstancesOutput, nbDataVolume)

	descripVolumesOutput, err := cmdLine.encrypter.CVS.DescribeVolumes(&ec2.DescribeVolumesInput{
		VolumeIds: vIDs,
	})
	assert.NoError(t, err)
	assert.Equal(t, len(descripVolumesOutput.Volumes), nbDataVolume)
	for _, volume := range descripVolumesOutput.Volumes {
		assert.True(t, *volume.Encrypted)
	}

	assert.False(t, flags.dryRunClean)
}

func getVolumeIDs(descripInstancesOutput *ec2.DescribeInstancesOutput, nbVolume int) []*string {
	volumeIDs := make([]*string, nbVolume)
	i := 0
	for _, instance := range descripInstancesOutput.Reservations[0].Instances {
		for _, block := range instance.BlockDeviceMappings {
			if *instance.RootDeviceName != *block.DeviceName {
				volumeIDs[i] = block.Ebs.VolumeId
				i++
			}
		}
	}
	return volumeIDs
}

func clean(t *testing.T, cvs ec2iface.EC2API, filters []*ec2.Filter, dryDelete bool) {
	if err := terminate(t, cvs, filters, dryDelete); err != nil {
		t.Fatal(err)
	}

	if err := deleteVolumes(t, cvs, filters, dryDelete); err != nil {
		t.Fatal(err)
	}

	if err := deleteSnapshots(t, cvs, filters, dryDelete); err != nil {
		t.Fatal(err)
	}

}

func terminate(t *testing.T, cvs ec2iface.EC2API, filters []*ec2.Filter, dryDelete bool) error {
	output, err := cvs.DescribeInstances(
		&ec2.DescribeInstancesInput{
			Filters: filters,
		},
	)
	if err != nil {
		return err
	}

	for _, resa := range output.Reservations {
		for _, instance := range resa.Instances {
			t.Logf("instance = %q", *instance.InstanceId)
			_, err := cvs.TerminateInstances(&ec2.TerminateInstancesInput{
				InstanceIds: []*string{instance.InstanceId},
				DryRun:      aws.Bool(dryDelete),
			})
			dryError := filterDryRunOperation(err)
			if !dryError && err != nil {
				return err
			}
			if !dryError {
				if err := cvs.WaitUntilInstanceTerminated(&ec2.DescribeInstancesInput{
					InstanceIds: []*string{instance.InstanceId},
					DryRun:      aws.Bool(dryDelete),
				}); !filterDryRunOperation(err) && err != nil {
					return err
				}
			}

		}
	}
	return nil
}

func deleteVolumes(t *testing.T, cvs ec2iface.EC2API, filters []*ec2.Filter, dryDelete bool) error {
	output, err := cvs.DescribeVolumes(
		&ec2.DescribeVolumesInput{
			Filters: filters,
		},
	)
	if err != nil {
		return err
	}

	for _, vol := range output.Volumes {
		t.Logf("volume = %q", *vol.VolumeId)
		if _, err := cvs.DeleteVolume(&ec2.DeleteVolumeInput{
			VolumeId: vol.VolumeId,
			DryRun:   aws.Bool(dryDelete),
		}); !filterDryRunOperation(err) && err != nil {
			return err
		}
	}
	return nil
}

func deleteSnapshots(t *testing.T, cvs ec2iface.EC2API, filters []*ec2.Filter, dryDelete bool) error {
	output, err := cvs.DescribeSnapshots(
		&ec2.DescribeSnapshotsInput{
			Filters: filters,
		},
	)
	if err != nil {
		return err
	}

	for _, snap := range output.Snapshots {
		t.Logf("snapshot = %q", *snap.SnapshotId)
		if _, err := cvs.DeleteSnapshot(&ec2.DeleteSnapshotInput{
			SnapshotId: snap.SnapshotId,
			DryRun:     aws.Bool(dryDelete),
		}); !filterDryRunOperation(err) && err != nil {
			return err
		}
	}
	return nil
}

//filterDryRunOperation if the code is dry run it returns nil
func filterDryRunOperation(err error) bool {
	const code = "DryRunOperation"
	awsErr, ok := err.(awserr.Error)
	if ok && awsErr.Code() == code {
		return true
	}
	return false
}
