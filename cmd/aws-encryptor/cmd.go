package main

import (
	"flag"
	"time"

	internalaws "bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/internal/aws"
	internalcmd "bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/internal/cmd"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/ec2"
)

//commandLine the arguments command line
type commandLine struct {
	internalcmd.CommandLine

	region    string
	accessKey string
	secretKey string
	roleARN   string
	profile   string
	fileName  string

	encrypter   *internalaws.Encrypter
	maxAttempts int
	waiterDelay time.Duration
	filters     map[string][]string
	tag         string

	encrypt bool
	yes     bool
}

func (c *commandLine) init() *commandLine {

	//flag
	c.Init("[aws-encryptor] ")

	flag.StringVar(&c.roleARN, "role-arn", "", "the role arn to assume")
	flag.StringVar(&c.region, "region", "", "the region")
	flag.StringVar(&c.accessKey, "access-key", "", "the access key")
	flag.StringVar(&c.secretKey, "secret-key", "", "the secret key")
	flag.StringVar(&c.profile, "profile", "", "the profile")
	flag.StringVar(&c.fileName, "filename", "", "the filename for the credential")
	flag.StringVar(&c.tag, "tag", "aws-encryptor", "the tag of execution")
	flag.BoolVar(&c.encrypt, "encrypt", false, "encrypt the volumes for each instance")
	flag.IntVar(&c.maxAttempts, "maxAttempts", 100, "maxAttempts for the wait after each actions")
	flag.DurationVar(&c.waiterDelay, "waiterDelay", 30*time.Second, "waiterDelay for the wait after each actions")
	flag.BoolVar(&c.yes, "yes", false, "yes if we want to continue")
	flag.Var(internalcmd.NewFiltersValue(map[string][]string{}, &c.filters), "filter", "the filter(s) -filter key1=value1,value2 -filter key2=value1,value2")

	flag.Parse()

	config := internalaws.CredentialsConfig{
		RoleARN:   c.roleARN,
		Region:    c.region,
		AccessKey: c.accessKey,
		SecretKey: c.secretKey,
		Filename:  c.fileName,
		Profile:   c.profile,
	}

	awsConfig := &aws.Config{
		DisableSSL: aws.Bool(false),
	}

	c.encrypter = &internalaws.Encrypter{
		CVS:         ec2.New(config.CreateSession(), awsConfig),
		Region:      c.region,
		Force:       c.yes,
		Tag:         c.tag,
		Encrypt:     c.encrypt,
		MaxAttempts: c.maxAttempts,
		WaiterDelay: c.waiterDelay,
	}

	return c

}

func (c *commandLine) main() int {
	err := c.encrypter.VolumesData(time.Now(), c.filters)
	if err != nil {
		return c.CommandLine.Fatal(err)
	}
	return 0
}
