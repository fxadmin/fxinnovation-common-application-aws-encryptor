SRC_DIR=bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor
#Default application 
APPL?=aws-encryptor
######## commom

PKGGOFILES=$(shell go list ./... | grep -v /vendor/)
CMD_TO_BUILD := ${sort ${dir ${wildcard ./cmd/*/}}}

GIT_COMMIT?=$(shell git rev-parse --short HEAD)
GIT_DIRTY?=$(shell test -n "`git status --porcelain`" && echo "+CHANGES" || true)
GIT_DESCRIBE?=$(shell git describe --tags --always)
BUILD_TIME?=$(shell date +"%Y-%m-%dT%H:%M:%S")

LDFLAGS=-ldflags "\
        -X $(SRC_DIR)/information.BuildTime=$(BUILD_TIME) \
        -X $(SRC_DIR)/information.GitCommit=$(GIT_COMMIT) \
        -X $(SRC_DIR)/information.GitDirty=$(GIT_DIRTY) \
        -X $(SRC_DIR)/information.GitDescribe=$(GIT_DESCRIBE)"


PWD=$(shell pwd)

BUILD_GOOS?=$(shell uname -s | tr '[:upper:]' '[:lower:]')
ifeq ($(BUILD_GOOS),windows)
  SUFFIX=".exe"
else
  SUFFIX=""
endif
BUILD_GOARCH?=amd64

.PHONY: help
help:
	@grep -hE '^[a-zA-Z_-]+.*?:.*?## .*$$' ${MAKEFILE_LIST} | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[0;49;95m%-30s\033[0m %s\n", $$1, $$2}'


## If you have go on your wonderful laptop
.PHONY: clean
clean:
	@mkdir -p ./target || true
	@rm -rf ./target/* || true

.PHONY: test
test: fmt vet ## go test
	go test -cpu=2 -p=2 -race -v --short $(LDFLAGS) $(PKGGOFILES)

.PHONY: test-it-test
test-it-test: fmt vet ## go test with integration
	go test $(PKGGOFILES) -cpu=2 -p=2 -race  -v $(LDFLAGS)

.PHONY: test-cover
test-cover: fmt vet ## go test with coverage
	go test  $(PKGGOFILES) -cover -v $(LDFLAGS) -covermode=count -coverprofile=coverage.out

.PHONY: test-coverage
test-coverage: clean fmt vet ## for jenkins
	gocov test $(PKGGOFILES) --short -cpu=2 -p=2 -v $(LDFLAGS) | gocov-xml > ./target/coverage-test.xml

.PHONY: build
build: clean fmt vet lint# build
	@$(foreach dir,$(CMD_TO_BUILD),CGO_ENABLED=0 GOOS=$(BUILD_GOOS) GOARCH=$(BUILD_GOARCH) go build -a -installsuffix cgo $(LDFLAGS) -o ./target/$(BUILD_GOARCH)_$(BUILD_GOOS)_$(shell basename $(dir))$(SUFFIX) $(dir) ;)

.PHONY: run
run: build ## Run command line
	go run $(LDFLAGS) ./cmd/${APPL}/*.go

.PHONY: fmt
fmt: ## go fmt on packages
	go fmt $(PKGGOFILES)

.PHONY: vet
vet: ## go vet on packages
	go vet $(PKGGOFILES)

.PHONY: lint
lint: ## go vet on packages
	golint -set_exit_status=true $(PKGGOFILES)

.PHONY: install
install: ## run 'go install' for each cmd
	@$(foreach dir,$(CMD_TO_BUILD),go install $(LDFLAGS) $(dir);)

.PHONY: vendor
vendor: ## run command line go vendor
	rm -rf Gopkg.lock vendor/
	dep ensure


.PHONY: tools
tools: ## install tools to develop
	go get -u github.com/golang/dep/cmd/dep
	go get -u github.com/golang/lint/golint
	go get github.com/axw/gocov/...
	go get github.com/AlekSi/gocov-xml
