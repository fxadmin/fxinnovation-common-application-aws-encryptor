// Package information on project
package information

import "fmt"

//We use ldflags
var (
	GitCommit   = "No GitCommit Provided"
	GitDescribe = "No GitDescribe Provided"
	GitDirty    = "No GitDirty Provided"
	BuildTime   = "No BuildTime Provided"
)

//Print information on project
func Print() string {
	return fmt.Sprintf("%q\t%q\t%q\t%q", BuildTime, GitCommit, GitDescribe, GitDirty)
}
