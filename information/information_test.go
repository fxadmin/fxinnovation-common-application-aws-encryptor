// Package information on project
package information_test

import (
	"testing"

	"bitbucket.org/fxadmin/fxinnovation-common-application-aws-encryptor/information"
	"github.com/stretchr/testify/assert"
)

func TestPrint(t *testing.T) {
	assert.NotEmpty(t, information.Print())
}
